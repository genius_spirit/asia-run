import React, { Fragment } from "react";
import { connect } from "react-redux";
import { NotificationContainer } from "react-notifications";
import 'react-notifications/lib/notifications.css';
import Navbar from "../../components/Navbar/Navbar";

const Layout = props => {
  return (
    <Fragment>
      <NotificationContainer/>
      <header>
        <Navbar/>
      </header>
      <main className="container">
        {props.children}
      </main>
      <footer>

      </footer>
    </Fragment>
  );
};

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};


export default connect(mapStateToProps, mapDispatchToProps)(Layout);
