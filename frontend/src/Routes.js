import React from "react";
import { Route, Switch } from "react-router-dom";
import MainPage from "./containers/MainPage/MainPage";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={MainPage} />
      <Route render={() => <h1 style={{ textAlign: "center", marginTop: '200px'}}>Page not found</h1>} />
    </Switch>
  );
};

export default Routes;
