import React, { Fragment } from "react";
import './Navbar.css';
import logo from '../../assets/logo.png'
import { NavLink } from "react-router-dom";
import { Icon } from "semantic-ui-react";

const Navbar = () => {
  return (
    <Fragment>
      <div className="Navbar-upperLine">
        <div className="container">
          <div className="Navbar-upperLine__icons">
            <NavLink className="Navbar-upperLine__text" to="https://www.facebook.com/BULGAKOVS.KG/">Bulgakov's</NavLink>
            <NavLink className="Navbar-upperLine__text" to="">О нас</NavLink>
            <a href="https://www.instagram.com/azia_beg/">
              <Icon name="instagram" size="large"/>
            </a>
            <a href="https://www.facebook.com/groups/1136515409705438/">
              <Icon name="facebook official" size="large"/>
            </a>
            <a href="">
              <Icon name="mail" size="large"/>
            </a>
            <NavLink to=""><Icon name="sign in" size="large"/></NavLink>
          </div>
        </div>
      </div>
      <div className="Navbar-logo">
        <div className="container">
          <img src={logo} alt="asiaRun logo" className="Navbar-logo__img" />
          <p>Беговой клуб в Кырзыстане</p>
        </div>
      </div>
      <div className="Navbar-nav">
        <div className="container">
          <NavLink className="Navbar-nav__menu" to="">Главная</NavLink>
          <NavLink className="Navbar-nav__menu" to="">Новости</NavLink>
          <NavLink className="Navbar-nav__menu" to="">Тренировки</NavLink>
          <NavLink className="Navbar-nav__menu" to="">Соревнования</NavLink>
          <NavLink className="Navbar-nav__menu" to="">Статьи</NavLink>
          <NavLink className="Navbar-nav__menu" to="">Видео</NavLink>
        </div>
      </div>

    </Fragment>
  );
};

export default Navbar;
