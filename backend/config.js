const prodDb = "mongodb://localhost:27017/asia-run";
const testDb = "mongodb://localhost:27017/asia-run-test";

module.exports = {
  db: {
    url: process.env.APP_ENV === "test" ? testDb : prodDb
  }
};