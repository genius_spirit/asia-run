const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NewsSchema = new Schema({
  title: {
    type: String,
    require: true
  },
  content: {
    type: String,
    required: true
  },
  image: String,
  dateTime: {
    type: Date,
    default: Date.now()
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }

});

const News = mongoose.model('News', NewsSchema);
module.exports = News;