const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require("./config");

const app = express();
const port = process.env.PORT || 8000;

app.use(cors());
app.use(express.json());

mongoose.connect(config.db.url, {useNewUrlParser: true });

const db = mongoose.connection;

db.once("open", () => {
    console.log("Mongoose connected");

    

    app.listen(port, (error) => {
        if (error) return console.error(`Server error ${error}`);
          console.log(`Server started on ${port} port!`);
    });
});


